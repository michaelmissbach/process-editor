<?php

namespace Missbach\ProcessEditorBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProcessEditorService
 * @package Missbach\ProcessEditorBundle\Services
 */
class ProcessEditorService
{
    const BUNDLE_NAME = 'Visual Process Editor';

    const AUTHOR = 'Mißbach Michael';

    const BUILD_DATE = '2018-06-19';

    const VERSION_MAJOR = 1;

    const VERSION_MINOR = 0;

    const VERSION_REV = 10;

    const VERSION_ADDITION = 'beta';

    /**
     * @return string
     */
    public static function getVersionString()
    {
        return sprintf(
            '%s.%s.%s%s',
            self::VERSION_MAJOR,
            self::VERSION_MINOR,
            self::VERSION_REV,
            strlen(self::VERSION_ADDITION)? sprintf(' %s',self::VERSION_ADDITION) : ''
        );
    }

    /**
     * @return string
     */
    public static function getCopyrightString()
    {
        return sprintf('Copyright %s (Build Date %s)',self::AUTHOR,self::BUILD_DATE);
    }

    /**
     * @return string
     */
    public static function getBundleNameString()
    {
        return sprintf('%s (Version %s)',self::BUNDLE_NAME,self::getVersionString());
    }

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ProcessExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $containerId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getBuildedContainerJs($containerId)
    {
        $view = '@MissbachProcessEditor/common/container_js_builder.html.twig';
        $args = ['container_id'=>$containerId];

        return $this->render($view,$args);
    }

    /**
     * @param $view
     * @param $args
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render($view,$args = [])
    {
        $content="";
        if ($this->container->has('templating')) {
            $content = $this->container->get('templating')->render($view, $args);
        } elseif ($this->container->has('twig')) {
            $content = $this->container->get('twig')->render($view, $args);
        }
        return $content;
    }
}
