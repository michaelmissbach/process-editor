<?php

namespace Missbach\ProcessEditorBundle\EventListener;

use Missbach\ProcessBundle\Services\ProcessService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Class ContainerIdListener
 * @package Missbach\ProcessEditorBundle\EventListener
 */
class ContainerIdListener
{
    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest()->request;
        if ($request->has('container_id') && $request->get('container_id')) {
            ProcessService::setCurrentContainerId($request->get('container_id'));
        }
    }
}
