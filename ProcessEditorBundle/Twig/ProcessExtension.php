<?php

namespace Missbach\ProcessEditorBundle\Twig;

use Missbach\ProcessBundle\Services\ProcessService;
use Missbach\ProcessEditorBundle\Services\ProcessEditorService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

/**
 * Class ProcessExtension
 * @package Missbach\ProcessBundle\Twig
 */
class ProcessExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ProcessExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'missbach.process.editor.twig';
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getProcessObjects', array($this, 'getProcessObjects'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getSavedObjects', array($this, 'getSavedObjects'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getSavedObject', array($this, 'getSavedObject'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getBuildedContainerJs', array($this, 'getBuildedContainerJs'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getBundleName', array($this, 'getBundleName'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getVersionString', array($this, 'getVersionString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getCopyrightString', array($this, 'getCopyrightString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getBundleNameString', array($this, 'getBundleNameString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getEditorVersionString', array($this, 'getEditorVersionString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getEditorCopyrightString', array($this, 'getEditorCopyrightString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getEditorBundleNameString', array($this, 'getEditorBundleNameString'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('getValueSave', array($this, 'getValueSave'), array('is_safe' => array('html')))
        ];
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('testfilter', array($this, 'testfilter')),
        );
    }

    /**
     * @return \Missbach\ProcessBundle\Core\Collections\ObjectCollection
     */
    public function getProcessObjects()
    {
        return $this->container->get('process.core.factory')->getAll();
    }

    /**
     * @param $containerId
     * @return mixed
     */
    public function getSavedObjects($containerId)
    {
        return $this->container->get('process.core.container')->get($containerId);
    }

    /**
     * @param $containerId
     * @param $elemId
     * @return mixed
     */
    public function getSavedObject($containerId,$elemId)
    {
        return $this->container->get('process.core.container')->getElementByContainerIdAndElementId($containerId,$elemId);
    }

    /**
     * @param $containerId
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function getBuildedContainerJs($containerId)
    {
        return $this->container->get('process.editor.service')->getBuildedContainerJs($containerId);
    }

    /**
     * @return string
     */
    public function getVersionString()
    {
        return ProcessService::getVersionString();
    }

    /**
     * @return string
     */
    public function getCopyrightString()
    {
        return ProcessService::getCopyrightString();
    }

    /**
     * @return string
     */
    public function getBundleNameString()
    {
        return ProcessService::getBundleNameString();
    }

    /**
     * @return string
     */
    public function getEditorVersionString()
    {
        return ProcessEditorService::getVersionString();
    }

    /**
     * @return string
     */
    public function getEditorCopyrightString()
    {
        return ProcessEditorService::getCopyrightString();
    }

    /**
     * @return string
     */
    public function getEditorBundleNameString()
    {
        return ProcessEditorService::getBundleNameString();
    }

    /**
     * @return string
     */
    public function getBundleName()
    {
        return ProcessService::BUNDLE_NAME;
    }

    /**
     * @param $data
     * @param $key
     * @return mixed|string
     */
    public function getValueSave($data,$key)
    {
        switch (true) {
            case is_array($data):
                return array_key_exists($key,$data) ? $data[$key] : '';
                break;
            case is_object($data):
                return property_exists($data,$key) ? $data->$key : '';
                break;
        }
        return '';
    }
}
