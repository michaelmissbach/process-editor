var VisualProcessEditor = {
    width: null,
    height: null,
    canvasObject: null,
    treeObject: null,
    leftWidth: 275,
    topHeight: 50,
    buttonsHeight: 40,
    bottomHeight: 100,
    maxLinesInOutput: 200,
    minLinesInOutput:100,
    init: function(containerId,firstload,about_template) {
        this.initCanvas(containerId);
        this.initTree();
        this.calculateLayout();
        var that = this;
        $(window).on('resize',function(){that.calculateLayout();});

        this.canvasObject.setEditor(this);
        this.treeObject.setEditor(this);

        this.canvasObject.load();
        this.treeObject.load();

        $(document).find('.scroll-pane').jScrollPane({autoReinitialise: true});
        $('.scroll-pane.hover-pane').mouseenter(function(e){
            $(this).find('.jspVerticalBar').fadeIn(200);
            $(this).find('.jspHorizontalBar').fadeIn(200);
        });
        $('.scroll-pane.hover-pane').mouseleave(function(e){
            $(this).find('.jspVerticalBar').fadeOut(200);
            $(this).find('.jspHorizontalBar').fadeOut(200);
        });

        if (firstload) {
            this.showAbout();
        }
    },
    showAbout: function() {
        this.information('Visual Process',$('#about').html());
    },

    initCanvas: function(containerId) {

        this.canvasObject = Object.create(Canvas);
        this.canvasObject.init('.canvas',containerId);
        this.canvasObject.canvas.height=4000;
        this.canvasObject.canvas.width=4000;

        var bg = Object.create(gridBackground);
        bg.init(this.canvasObject.canvas,50,50,'#bac6c6','#939394');
        this.canvasObject.addBackgroundDrawables(bg);
    },
    initTree: function(canvasObject) {
        this.treeObject = Object.create(VisualProcessDataTree);
        this.treeObject.init();
    },
    calculateLayout: function() {

        this.width = $(window).width();//window.innerWidth;
        this.height = $(window).height();//window.innerHeight;

        $('.vp-main').width(this.width);
        $('.vp-main').height(this.height);

        $('.vp-top').height(this.topHeight);
        $('.vp-bottom').height(this.bottomHeight);

        $('.vp-right').width(this.width - this.leftWidth);
        $('.vp-left').width(this.leftWidth);

        $('.vp-right').height(this.height - this.topHeight);
        $('.vp-right .vp-buttons').height(this.buttonsHeight);
        $('.vp-right .vp-canvas').height(this.height - this.topHeight - this.buttonsHeight);

        $('.vp-left .vp-buttons').height(this.buttonsHeight);
        $('.vp-tree').height(this.height - this.topHeight - this.buttonsHeight - this.bottomHeight);



        $('.border').each(function(e,i) {
            var borderWidth = parseInt($(this).css('border-left-width')) + parseInt($(this).css('border-right-width'));
            var borderHeight = parseInt($(this).css('border-top-width')) + parseInt($(this).css('border-bottom-width'));
            var parent = $(this).parent();
            $(this).width(parent.width() - borderWidth);
            $(this).height(parent.height() - borderHeight);
        });
    },
    information: function(title,message,callback) {
        $( "#dialog-inform").attr('title',title);
        $( "#dialog-inform .text").html(message);
        $( ".ui-dialog .ui-dialog-title" ).html(title);
        $( "#dialog-inform" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Ok": function() {
                    $( this ).dialog( "close" );
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            }
        });
    },
    confirmation: function(title,message,callback) {
        $( "#dialog-confirm").attr('title',title);
        $( "#dialog-confirm .text").html(message);
        $( ".ui-dialog .ui-dialog-title" ).html(title);
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Ok": function() {
                    $( this ).dialog( "close" );
                    if (typeof callback === 'function') {
                        callback();
                    }
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    },
    form: function(title,message,fields,callback) {
        $( "#dialog-form").attr('title',title);
        $( "#dialog-form .text").html(message);
        $( ".ui-dialog .ui-dialog-title" ).html(title);

        $( "#dialog-form form fieldset").html('');
        for (var i=0;i < fields.length;i++) {
            var obj = fields[i];
            $( "#dialog-form form fieldset").append('<div><label>'+obj.label+' </label><input type="'+obj.type+'" value="'+obj.value+'" name="'+obj.name+'"/></div>');
        }

        $( "#dialog-form" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Ok": function() {
                    $( this ).dialog( "close" );
                    var f = $( "#dialog-form form").serializeArray();
                    var result = {};
                    for(var i = 0; i < f.length;i++) {
                        if (typeof f[i] === 'object') {
                            result[f[i].name] = f[i].value
                        }
                    }
                    if (typeof callback === 'function') {
                        callback(result);
                    }
                },
                Cancel: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    },
    editorform: function(title,message,form,callback) {
        var that = this;

        $( "#dialog-editor-form").attr('title',title);
        $( "#dialog-editor-form .text").html(message);
        $( ".ui-dialog .ui-dialog-title" ).html(title);

        $( "#dialog-editor-form .wrapper").html(form);

        window.setTimeout(function(){
            if (typeof this['editor_construct'] === 'function') {
                editor_construct();
            }
        },250);

        $( "#dialog-editor-form" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Ok": function() {
                    try {
                        editor_destruct();
                    } catch(e) {}
                    var f = $( "#dialog-editor-form .wrapper form").serializeArray();
                    var result = {};
                    for(var i = 0; i < f.length;i++) {
                        if (typeof f[i] === 'object') {
                            result[f[i].name] = f[i].value
                        }
                    }
                    $( "#dialog-editor-form .wrapper" ).html('');
                    $( this ).dialog( "close" );
                    if (typeof callback === 'function') {
                        callback(result);
                    }
                },
                Cancel: function() {
                    try {
                        editor_destruct();
                    } catch(e) {}
                    $( "#dialog-editor-form .wrapper" ).html('');
                    $( this ).dialog( "close" );
                }
            }
        });
    },
    output: function(message) {
        switch (true) {
            case typeof message === 'object' && Array.isArray(message):
                var stringifiedMessage = '';
                for (index = 0; index < message.length; ++index) {
                    stringifiedMessage += message[index];
                    if(index < message.length -1) {
                        stringifiedMessage += '<br>';
                    }
                }
                this.output(stringifiedMessage);
                break;
            case typeof message === 'string':
                $('.vp-bottom .output-text p').append(message);
                $('.vp-bottom .output-text p').append('<br>');
                //$('.vp-bottom .output-text').animate({ scrollTop: $('.vp-bottom .output-text').prop("scrollHeight")}, 1000);
                window.setTimeout(function(){
                    $('.vp-bottom .output-text').data('jsp').scrollToBottom();
                },500);
                break;
        }
        if($('.vp-bottom .output-text p br').length > this.maxLinesInOutput) {
            var tmp = $('.vp-bottom .output-text p').html().split('<br>');
            for (var i = 0; i < this.minLinesInOutput; i++) {
                tmp.shift();
            }
            $('.vp-bottom .output-text p').html(tmp.join('<br>'));
            //$('.vp-bottom .output-text').animate({ scrollTop: $('.vp-bottom .output-text').prop("scrollHeight")}, 1000);
            window.setTimeout(function(){
                $('.vp-bottom .output-text').data('jsp').scrollToBottom();
            },500);
        }
    },
    interact: function(url,params,callback) {
        var that = this;
        this.canvasObject.lockedByAjax();
        params.container_id = this.canvasObject.containerId;
        $.ajax({
            method: "POST",
            url: url,
            data: params,
            error: function(data){
                    that.output(data.statusText);
                }
            })
            .done(function( msg ) {
                if (typeof callback === "function") {
                    callback(msg);
                }
                that.canvasObject.unlockedByAjax();
            });
    },
};

var VisualProcessDataTree = {
    controls: null,
    tree:null,
    editor: null,
    setEditor: function (editor){
        this.editor = editor;
    },

    init: function() {
        var that = this;
        this.tree = $('.vp-tree .tree');

        this.controls = Object.create(VisualProcessGenericControlContainer);
        this.controls.init(
            '.vp-left .vp-buttons .controls',
            null,
            function() {
                for(var i=0;i<this.controls.length;i++) {
                    var elem = this.controlContainer.find('ul li[data-function="'+this.controls[i].functionname+'"]');
                    elem.attr('clickable','none-clickable-function');
                    if (inArray('all',this.controls[i].additional)) {
                        elem.attr('clickable','clickable-function');
                    }
                    if (inArray('node',this.controls[i].additional)) {
                        if (that.hasSelectedNodeChildren()) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                    if (inArray('leaf',this.controls[i].additional)) {
                        if (!that.hasSelectedNodeChildren()) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                    if (inArray('noroot',this.controls[i].additional)) {
                        if (that.hasSelectedNodeParent()) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                    if (inArray('modified',this.controls[i].additional)) {
                        if (that.isSelectedNodeModified()) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                }
            });
        this.controls.addControl('add',this,this.addchildNode,'fas fa-plus fa-lg','Add',['all']);
        this.controls.addControl('remove',this,this.removeNode,'far fa-trash-alt fa-lg','Remove',['noroot']);
        this.controls.addControl('rename',this,this.renameNode,'far fa-edit fa-lg','Rename',['noroot']);
        this.controls.addControl('save',this,this.saveNode,'far fa-save fa-lg','Save',['noroot']);
        this.controls.addControl('load',this,this.loadNode,'fas fa-upload fa-lg','Load',['noroot']);
        this.controls.addControl('execute',this,this.execute,'far fa-play-circle fa-lg','Execute',['noroot']);
        this.controls.build();

        this.tree.jstree({
            "core": {
                'data': {
                    'url': 'tree/loadall'
                },
                "animation": 0,
                "themes": {
                    "variant": "small"
                },
                "check_callback" : function(operation, node, node_parent, node_position, more) {
                    // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                    // in case of 'rename_node' node_position is filled with the new node name

                    /*if (operation === "move_node") {
                        return node_parent.original.type === "Parent"; //only allow dropping inside nodes of type 'Parent'
                    }*/
                    return true;
                }
            },
            "plugins": [
                /*"dnd",
                "search",
                "state",
                "types",*/
                "wholerow"
            ]
        }).bind(
            "select_node.jstree", function (evt, data) {
                that.editor.canvasObject.containerId = data.node.id;
                that.editor.canvasObject.load();
                that.controls.updateVisibility();
            });
    },
    load: function() {
        var that = this;
        var callback = function(msg) {
            that.getTree().jstree(true).settings.core.data = msg;
            that.getTree().jstree(true).refresh();
        };
        this.interact('tree/loadall',{},callback);
    },
    getTree: function() {
        return this.tree;
    },
    getSelectedNode: function() {
        return this.getTree().jstree('get_selected',true);
    },
    getSelectedNodeName: function() {
        return this.getTree().jstree('get_selected');
    },
    hasSelectedNodeParent: function() {
        try {
            var node = this.getSelectedNode();
            return node[0].parent !== '#';
        } catch (e) {
        }
        return false;
    },
    getSelectedNodeId: function() {
        var node = this.getSelectedNode();
        return node[0].id;
    },
    hasSelectedNodeChildren: function() {
        var node = this.getSelectedNode();
        return node[0].children.length > 0;
    },
    isSelectedNodeModified: function() {
        var node = this.getSelectedNode();
        return node[0].state.modified;
    },
    setDirty: function() {
        var node = this.getSelectedNode();
        var name = this.getSelectedNodeName();
        if (name[0].lastIndexOf('*') < 0) {
            this.getTree().jstree('rename_node', this.getSelectedNodeName() , node[0].state.shownName + ' *' );
        }
        var node = this.getSelectedNode();
        node[0].state.modified = true;
    },
    interact: function(url,params,callback) {
        this.editor.interact(url,params,callback);
    },
    addchildNode: function(tree) {
        var that = tree;
        var callback = function() {
            that.editor.output('Added container "'+tree.getSelectedNodeId()+'"');
            that.load();
        };
        tree.interact('tree/add',{'element_id':tree.getSelectedNodeId()},callback);
    },
    removeNode: function(tree) {
        var that = tree;
        var callback = function() {
            var callbackinner = function() {
                that.editor.output('Removed container "'+tree.getSelectedNodeId()+'"');
                that.editor.canvasObject.containerId = null;
                that.editor.canvasObject.disabled = true;
                that.editor.canvasObject.updateElementStatus();
                that.load();

            };
            tree.interact('tree/remove',{'element_id':tree.getSelectedNodeId()},callbackinner);
        };

        tree.editor.confirmation('Delete Container','Are you sure?',callback);
    },
    renameNode: function(tree) {
        var that = tree;
        var callback = function(form) {
            var callbackinner = function() {
                that.editor.output('Renamed container "'+tree.getSelectedNodeId()+'"');
                that.load();
            };
            tree.interact('tree/rename',form,callbackinner);
        };

        var node = that.getSelectedNode();
        tree.editor.form('Edit properties','',[{'label':'Title','type':'text','name':'shown_name','value':node[0].state.shownName},{'label':'Internal Name','type':'text','name':'call_name','value':node[0].state.callName}],callback);
    },
    saveNode: function(tree) {

        var that = tree;
        var callback = function() {
            var callbackinner = function() {
                that.editor.output('Saved container "'+tree.getSelectedNodeId()+'"');
                that.load();
            };
            tree.interact('tree/save',{'element_id':tree.getSelectedNodeId()},callbackinner);
        };

        tree.editor.confirmation('Save Container','Are you sure?',callback);

    },
    loadNode: function(tree) {
        var that = tree;
        var callback = function() {
            var callbackinner = function() {
                that.editor.output('loaded container "'+tree.getSelectedNodeId()+'"');
                that.load();
            };
            tree.interact('tree/load',{'element_id':tree.getSelectedNodeId()},callbackinner);
        };

        tree.editor.confirmation('Load Container','Are you sure?',callback);
    },
    execute: function(tree) {
        var that = tree;
        var callback = function(msg) {
            var message = 'The output result is "'+(msg.result.result === true ? 'true':'false')+'".<br>';
            if (msg.result.error) {
                message += 'An error occured: '+msg.result.error+'.<br>';
            }
            message += '<br>';
            message += 'Trace:<br>';
            message += '<span class="trace">';
            for (var i=0;i < msg.result.trace.length;i++) {
                message += msg.result.trace[i]+'<br>';
            }
            message += '</span>';

            tree.editor.information('Check process',message);

        };
        tree.interact('test/process',{},callback);
    },

    /*selectNode: function (id) {
        this.getTree().jstree('deselect_all');
        var node = this.getTree().find('[data-id="'+id+'"]');
        this.getTree().jstree("select_node", $(node).attr('id'));
    },*/
};

var Canvas = {
    controls: null,
    disabled:false,
    containerId:null,
    selector:null,
    container:null,
    drawables: [],
    backgroundDrawables: [],
    interactiveDrawables: [],
    canvas: null,
    ctx: null,
    dirty: false,
    interval: 16,
    fallbackInterval: 2000,
    stylePaddingLeft: 0,
    stylePaddingTop: 0,
    styleBorderLeft: 0,
    styleBorderTop: 0,
    htmlTop: 0,
    htmlLeft : 0,
    dragging: false,
    dragoffx: 0,
    dragoffy: 0,
    dragstartx: 0,
    dragstarty: 0,
    selection: null,
    selectionColor: '#CC0000',
    selectionWidth: 2,
    drawLinesColor: 'blue',
    resolvableContextMenuElements:null,
    unresolvableContextMenuElements:null,
    modules: null,
    editor: null,
    setEditor: function (editor){
        this.editor = editor;
    },
    init: function(selector,containerId) {
        if(typeof selector === 'undefined') {
            selector = 'body';
        }
        this.selector = selector;
        this.container = $(selector);
        this.containerId = containerId;

        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.disabled = true;
        this.updateElementStatus();

        $(selector).append(this.canvas);

        var that = this;
        this.canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
        this.canvas.addEventListener('mousedown', function(e){that.mousedown(e)},true);
        this.canvas.addEventListener('mousemove', function(e){that.mousemove(e)},true);
        this.canvas.addEventListener('mouseup', function(e){that.mouseup(e)},true);

        this.initMobile();

        this.container.on('scroll', function(e){that.dirty=true;});

        setInterval(function() { that.draw(); }, this.interval);
        setInterval(function() { that.dirty = true; }, this.fallbackInterval);

        this.modules = [];
        this.unresolvableContextMenuElements = {};
        this.resolvableContextMenuElements = {};
        this.contextMenu();
        this.initBoundaries();

        this.controls = Object.create(VisualProcessGenericControlContainer);
        this.controls.init(
            '.vp-right .vp-buttons .controls',
            null,
            function() {
                for(var i=0;i<this.controls.length;i++) {
                    var elem = this.controlContainer.find('ul li[data-function="'+this.controls[i].functionname+'"]');
                    elem.attr('clickable','none-clickable-function');

                    if (inArray('selected_element',this.controls[i].additional)) {
                        if (this.controls[i].context.selection) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                    if (inArray('editable_element',this.controls[i].additional)) {
                        if (this.controls[i].context.selection && this.controls[i].context.isSelectionEditable()) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                    if (inArray('none_selected',this.controls[i].additional)) {
                        if (!this.controls[i].context.selection) {
                            elem.attr('clickable','clickable-function');
                        }
                    }
                }
            });
    },

    initMobile: function() {
        var that=this;
        this.canvas.addEventListener("touchstart", function (e) {
            mousePos = getTouchPos(that.canvas, e);
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousedown", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            that.canvas.dispatchEvent(mouseEvent);
        }, false);
        this.canvas.addEventListener("touchend", function (e) {
            var mouseEvent = new MouseEvent("mouseup", {});
            that.canvas.dispatchEvent(mouseEvent);
        }, false);
        this.canvas.addEventListener("touchmove", function (e) {
            var touch = e.touches[0];
            var mouseEvent = new MouseEvent("mousemove", {
                clientX: touch.clientX,
                clientY: touch.clientY
            });
            that.canvas.dispatchEvent(mouseEvent);
        }, false);

        document.body.addEventListener("touchstart", function (e) {
            if (e.target == that.canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchend", function (e) {
            if (e.target == that.canvas) {
                e.preventDefault();
            }
        }, false);
        document.body.addEventListener("touchmove", function (e) {
            if (e.target == that.canvas) {
                e.preventDefault();
            }
        }, false);

        // Get the position of a touch relative to the canvas
        function getTouchPos(canvasDom, touchEvent) {
            var rect = canvasDom.getBoundingClientRect();
            return {
                x: touchEvent.touches[0].clientX - rect.left,
                y: touchEvent.touches[0].clientY - rect.top
            };
        }
    },

    initBoundaries: function() {
        this.htmlTop = document.body.parentNode.offsetTop;
        this.htmlLeft = document.body.parentNode.offsetLeft;
        if (document.defaultView && document.defaultView.getComputedStyle) {
            this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(this.canvas, null)['paddingLeft'], 10)      || 0;
            this.stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(this.canvas, null)['paddingTop'], 10)       || 0;
            this.styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(this.canvas, null)['borderLeftWidth'], 10)  || 0;
            this.styleBorderTop   = parseInt(document.defaultView.getComputedStyle(this.canvas, null)['borderTopWidth'], 10)   || 0;
        }
    },

    interact: function(url,params,callback) {
        this.editor.interact(url,params,callback);
    },
    lockedByAjax: function() {
        this.container.addClass('locked');
    },
    unlockedByAjax: function() {
        this.container.removeClass('locked');
    },
    load: function() {
        if (this.editor.treeObject.hasSelectedNodeParent()) {
            var that = this;
            this.interact(
                'edit/load',
                {},
                function(msg) {
                    that.editor.output('Loaded container "'+that.containerId+'".');
                    that.interactiveDrawables = [];
                    that.drawables = [];
                    that.selection = null;
                    eval(msg.script);
                    that.disabled = false;
                    that.updateElementStatus();
                    that.dirty = true;
                });
        } else {
            this.disabled = true;
            this.updateElementStatus();
        }
    },

    updateElementStatus: function() {
        if (this.disabled) {
            $(this.selector).find('canvas').hide();
            if (this.controls) {
                this.controls.clearControls();
                this.controls.build();
            }
        } else {
            $(this.selector).find('canvas').show();
        }
    },

    contextMenu: function() {
        var that = this;
        $.contextMenu({
            selector: this.selector + ' canvas',
            build: function($trigger, e) {
                window.setTimeout(function(){
                    if (that.selection) {
                        $('.context-menu-root').addClass('data-title');
                        $('.data-title').attr('data-menutitle', that.selection.elementId);
                    } else {
                        $('.context-menu-root').removeClass('data-title');
                    }
                },100);
                return {
                    callback: function(key, options) {
                        if(key.startsWith('__')) {
                            if (key === "__delete") {
                                that.deleteElement(that);
                            }
                            if (key === "__edit") {
                                that.showEditor(that);
                            }
                        } else {
                            that.addElement(key,that.getMouse(e));
                        }
                    },
                    items: that.getContextMenuItems()
                };
            }
        });
    },

    showElementGui: function(context) {
        var that = context;
        var callback = function (msg) {
            if (msg.objectname) {
                var obj = $(that.selector);
                var sl = that.container.data('jsp').getContentPositionX();//this.container.scrollLeft();
                var st = that.container.data('jsp').getContentPositionY();//this.container.scrollTop();
                that.addElement(msg.objectname,{x: parseInt(sl + (obj.width()/2)), y: parseInt(st + (obj.height()/2))});
            }
        };
        context.interact(
            'elements/load',
            {}
            ,
            function (msg) {
                that.editor.editorform('Add','Add Element:',msg.form,callback);
            });
    },

    addElement: function(key,pos){
        var that = this;
        this.interact(
            'edit/add_element',
            {
                name:key,
                pos:pos
            },
            function(msg) {
                that.editor.treeObject.setDirty();
                that.addContainerElementFromName(key,pos,msg.id);
                that.editor.output('Added element "'+key+'" with id "'+msg.id+'"');
            });
    },

    deleteElement: function(context) {
        var that = context;
        var callbackInner = function() {
            that.editor.output('Deleted element id "'+that.selection.elementId+'"');
            that.interact(
                'edit/remove_element',
                {
                    elemId: that.selection.elementId
                },
                function (msg) {
                    that.editor.treeObject.setDirty();
                    that.load();
                });
        };
        VisualProcessEditor.confirmation('Delete Process Object','Are you sure?',callbackInner);
    },

    showEditor: function(context) {
        var that = context;
        var callback = function (msg) {
            that.interact(
                'editor/save',
                {
                    element_id: that.selection.elementId,
                    properties: msg
                },
                function () {
                    that.editor.treeObject.setDirty();
                });
        };
        context.interact(
            'editor/load',
            {
                element_id: that.selection.elementId
            },
            function (msg) {
                that.editor.editorform('Edit','Edit values:',msg.form,callback);
            });
    },

    getContextMenuItems: function() {
        var menu = {};

        if (this.isSelectionEditable()) {
            menu.__edit ={name: "Edit", disabled: this.selection === null };
        }
        menu.__delete = {name: "Delete",  disabled: this.selection === null };
        menu.sep1 = "---------";
        menu.unresolvable = {name:"Elements",items: this.unresolvableContextMenuElements, disabled: this.selection !== null};
        menu.resolvable =   {name:"Resolver",items: this.resolvableContextMenuElements, disabled: this.selection !== null};
        return menu;
    },

    isSelectionEditable: function() {
        var editable = false;

        if (this.selection !== null) {
            var module = this.getModuleByIdentification(this.selection.identification);
            editable = module.editable;
        }

        return editable;
    },

    getModuleByIdentification: function(name) {
        for (var i = 0; i < this.modules.length; i++) {
            if (name === this.modules[i].identification) {
                return this.modules[i];
            }
        }
    },

    addContainerElementFromName: function(name,pos,id) {

        for (var i = 0; i < this.modules.length; i++) {
            if (this.modules[i].identification === name) {
                if (typeof id === 'undefined') {
                    id = Canvas.guid();
                }
                if(this.modules[i].isresolvable) {
                    var elem = Object.create(interactiveResolvableElement);
                    elem.init(
                        this,
                        this.modules[i].identification,
                        name,
                        this.modules[i].options.inputs,
                        this.modules[i].options.outputs,
                        pos.x,
                        pos.y,
                        this.modules[i].options.inputNames,
                        this.modules[i].options.outputNames
                    );
                } else {
                    var elem = Object.create(interactiveUnResolvableElement);
                    elem.init(
                        this,
                        this.modules[i].identification,
                        name,
                        pos.x,
                        pos.y
                    );
                }

                this.addInteractiveDrawables(elem,id);
                return;
            }
        }
    },

    clearModules: function() {
        this.modules = [];
    },
    addModule: function(options) {
        options = JSON.parse(options);
        var m = Object.create(module);
        m.init(options);
        this.modules.push(m);
        this.addContextMenuEntry(options.category,options.name,options.isresolvable);
    },

    activateModules: function() {
        this.controls.clearControls();
        this.controls.addControl('add',this,this.showElementGui,'fas fa-plus fa-lg','Add',['none_selected']);
        this.controls.addControl('edit',this,this.showEditor,'far fa-edit fa-lg','Edit',['editable_element']);
        this.controls.addControl('remove',this,this.deleteElement,'far fa-trash-alt fa-lg','Remove',['selected_element']);
        this.controls.build();
        this.controls.updateVisibility();
    },

    addContextMenuEntry: function(category,name,isresolvable) {

        if (category.length) {
            if (isresolvable) {
                if (!this.resolvableContextMenuElements.hasOwnProperty(category)) {
                    this.resolvableContextMenuElements[category] = {name:category};
                    this.resolvableContextMenuElements[category]['items'] = {};
                    this.resolvableContextMenuElements[category]['items'][name] = {};
                }
                this.resolvableContextMenuElements[category]['items'][name]={name: name};
            } else {
                if (!this.unresolvableContextMenuElements.hasOwnProperty(category)) {
                    this.unresolvableContextMenuElements[category] = {name:category};
                    this.unresolvableContextMenuElements[category]['items'] = {};
                    this.unresolvableContextMenuElements[category]['items'][name] = {};
                }
                this.unresolvableContextMenuElements[category]['items'][name]={name: name};
            }
        }
    },

    addBackgroundDrawables: function(e) {
        this.backgroundDrawables[Canvas.guid()] = e;
        this.dirty = true;
    },

    addDrawables: function(e) {
        this.drawables[Canvas.guid()] = e;
        this.dirty = true;
    },

    addInteractiveDrawables: function(e,id) {
        this.interactiveDrawables[id] = e;
        e.elementId = id;
        this.dirty = true;
    },

    guid: function() {
        return Canvas.s4() + Canvas.s4() + '-' + Canvas.s4() + '-' + Canvas.s4() + '-' +
            Canvas.s4() + '-' + Canvas.s4() + Canvas.s4() + Canvas.s4();
    },
    s4: function() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    },

    clear: function() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.dirty = true;
    },

    draw: function() {
        if(!this.dirty) {
            return;
        }
        var drawCount = 0;
        this.clear();

        //draw background
        for (key in this.backgroundDrawables) {
            var d = this.backgroundDrawables[key];
            d.draw(this.ctx);
        }
        //draw interactive
        for (key in this.interactiveDrawables) {
            var d = this.interactiveDrawables[key];
            if (!this.IntersectsFrustum(d)) continue;
            drawCount++;
            try {
                d.draw(this.ctx);
            } catch(e){}

        }
        //draw interactive lines
        for (key in this.interactiveDrawables) {
            var interactiveDrawable = this.interactiveDrawables[key];
            if (interactiveDrawable.type === "resolvable") {
                var inputs = interactiveDrawable.inputs;
                for(var ii = 0;ii < inputs.length;ii++) {
                    if(inputs[ii] !== null) {
                        if (inputs[ii].type==='out') {
                            interactiveDrawable.getConnectorInPosition(ii+1);
                            var fromx = vector2.x;
                            var fromy = vector2.y;
                            this.interactiveDrawables[inputs[ii].elementId].getConnectorOutPosition(inputs[ii].num);
                            var tox = vector2.x;
                            var toy = vector2.y;

                            this.ctx.beginPath();
                            this.ctx.strokeStyle = this.drawLinesColor;
                            this.drawArrow(fromx,fromy,tox,toy);
                            //this.ctx.quadraticCurveTo(fromx,fromy,tox,toy);
                            this.ctx.stroke();
                        }
                    }
                }
            } else if (interactiveDrawable.type === "unresolvable") {
                //todo


                let directions = ['north','south','west','east'];
                for (let i in directions) {

//console.log(interactiveDrawable.getConnection(directions[i]));
                    if (interactiveDrawable.getConnection(directions[i]).elementId && interactiveDrawable.getConnection(directions[i]).type === 'out') {
                        interactiveDrawable.getConnectorPosition(directions[i]);
                        var fromx = vector2.x;
                        var fromy = vector2.y;
                        let target = interactiveDrawable.getConnection(directions[i]).elementId;

                        this.interactiveDrawables[target].getConnectorPosition(interactiveDrawable.getConnection(directions[i]).direction);
                        var tox = vector2.x;
                        var toy = vector2.y;



                        this.ctx.beginPath();
                        this.ctx.strokeStyle = this.drawLinesColor;
                        this.drawArrow(fromx,fromy,tox,toy);
                        //this.ctx.quadraticCurveTo(fromx,fromy,tox,toy);
                        this.ctx.stroke();
                    }

                }



                /*let directions = ['north','south','west','east'];
                for (let i in directions) {

                    if (interactiveDrawable.getConnection(directions[i])) {
                        interactiveDrawable.getConnectorPosition(directions[i]);
                        var fromx = vector2.x;
                        var fromy = vector2.y;
                        let target = interactiveDrawable.getConnection(directions[i]);
                        console.log(target);
                        for (let ii in directions) {



                            if (this.interactiveDrawables[target].getConnection(directions[ii]) === interactiveDrawable.elementId) {
                                console.log('found');
                                this.interactiveDrawables[target].getConnectorPosition(directions[ii]);
                                var tox = vector2.x;
                                var toy = vector2.y;

                                this.ctx.beginPath();
                                this.ctx.strokeStyle = this.drawLinesColor;
                                this.drawArrow(fromx,fromy,tox,toy);
                                //this.ctx.quadraticCurveTo(fromx,fromy,tox,toy);
                                this.ctx.stroke();

                            }

                        }
                    }


                }*/
            }

        }
        //draw selection again to put in the foreground
        if (this.selection != null) {
            this.selection.draw(this.ctx);
            this.ctx.strokeStyle = this.selectionColor;
            this.ctx.lineWidth = this.selectionWidth;
            var mySel = this.selection;
            this.drawRoundRect(mySel.x,mySel.y,mySel.w,mySel.h,10)
            this.ctx.lineWidth = 1;
        }
        //draw the rest
        for (key in this.drawables) {
            var d = this.drawables[key];
            if (!this.IntersectsFrustum(d)) continue;
            drawCount++;
            d.draw(this.ctx);
        }
        this.dirty = false;
        //console.log('draws: '+drawCount);
    },

    drawArrow: function(tox, toy, fromx, fromy){
        var headlen = 10;
        var angle = Math.atan2(toy-fromy,tox-fromx);
        this.ctx.moveTo(fromx, fromy);
        this.ctx.lineTo(tox, toy);
        this.ctx.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
        this.ctx.moveTo(tox, toy);
        this.ctx.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
    },
    drawRoundRect: function (x, y, width, height, radius, fill, stroke) {
        if (typeof stroke == "undefined" ) {
            stroke = true;
        }
        if (typeof radius === "undefined") {
            radius = 5;
        }
        this.ctx.beginPath();
        this.ctx.moveTo(x + radius, y);
        this.ctx.lineTo(x + width - radius, y);
        this.ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
        this.ctx.lineTo(x + width, y + height - radius);
        this.ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
        this.ctx.lineTo(x + radius, y + height);
        this.ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
        this.ctx.lineTo(x, y + radius);
        this.ctx.quadraticCurveTo(x, y, x + radius, y);
        this.ctx.closePath();
        if (stroke) {
            this.ctx.stroke();
        }
        if (fill) {
            this.ctx.fill();
        }
    },

    forceRedraw: function() {
        this.dirty = true;
    },
    connectUnResolvableElements: function(interact,elementId1,direction1, elementId2,direction2) {
        //todo
        /*console.log(elementId1);
        console.log(direction1);
        console.log(elementId2);
        console.log(direction2);*/

        var inputCurrent = this.interactiveDrawables[elementId1].getConnection(direction2);
        var outputCurrent = this.interactiveDrawables[elementId2].getConnection(direction1);

        //console.log(inputCurrent);
        //console.log(outputCurrent);

        if (!inputCurrent.elementId && !outputCurrent.elementId) {
            this.interactiveDrawables[elementId1].setConnection(direction1,elementId2,direction2,'out');
            this.interactiveDrawables[elementId2].setConnection(direction2,elementId1,direction1,'in');
        }

    },
    connectResolvableElements: function(interact,elementId1,input, elementId2,output) {
        var that = this;
        var inputCurrent = this.interactiveDrawables[elementId1].getConnectorInput(input);
        var outputCurrent = this.interactiveDrawables[elementId2].getConnectorOutput(output);

        if (inputCurrent && inputCurrent.elementId === elementId2 && inputCurrent.num === output &&
            outputCurrent && outputCurrent.elementId === elementId1 && outputCurrent.num === input) {
            this.interactiveDrawables[elementId1].setConnectorInput(input);
            this.interactiveDrawables[elementId2].setConnectorOutput(output);
            if (interact) {
                this.interact(
                    'edit/unconnect_elements',
                    {
                        'elemId1':elementId2,
                        'elemId2':elementId1,
                        'outputChannel':output,
                        'inputChannel':input
                    },
                    function (msg) {
                        that.editor.treeObject.setDirty();
                    });
            }
            this.dirty = true;
        } else if (!inputCurrent && !outputCurrent) {
            this.interactiveDrawables[elementId1].setConnectorInput(input,elementId2,output);
            this.interactiveDrawables[elementId2].setConnectorOutput(output,elementId1,input);
            if (interact) {
                this.interact(
                    'edit/connect_elements',
                    {
                        'elemId1':elementId2,
                        'elemId2':elementId1,
                        'outputChannel':output,
                        'inputChannel':input
                    },
                    function (msg) {
                        that.editor.treeObject.setDirty();
                    });
            }
            this.dirty = true;
        }
    },

    getMouse: function(e) {
        var related = this.relatedPosition();
        mx = e.pageX - related.x;
        my = e.pageY - related.y;
        return {x: mx, y: my};
    },

    relatedPosition: function() {
        var element = this.canvas, offsetX = 0, offsetY = 0, mx, my;
        if (element.offsetParent !== undefined) {
            do {
                offsetX += element.offsetLeft;
                offsetY += element.offsetTop;
            } while ((element = element.offsetParent));
        }
        offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft - this.container.scrollLeft();
        offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop - this.container.scrollTop();

        return {x: offsetX, y: offsetY};
    },

    IntersectsFrustum: function(d) {
        var cw = this.container[0].offsetWidth;
        var ch = this.container[0].offsetHeight;
        var sl = this.container.data('jsp').getContentPositionX();//this.container.scrollLeft();
        var st = this.container.data('jsp').getContentPositionY();//this.container.scrollTop();
        var frustum = {xmin:sl,xmax:sl+cw,ymin:st,ymax:st+ch};
        var obj = {xmin:d.x,xmax:d.x+d.w,ymin:d.y,ymax:d.y+d.h};
        return (obj.xmax >= frustum.xmin && obj.xmin <= frustum.xmax && obj.ymax >= frustum.ymin && obj.ymin <= frustum.ymax);
    },

    mousedown: function(e) {
        var mouse = this.getMouse(e);
        var mx = mouse.x;
        var my = mouse.y;

        for (i in this.interactiveDrawables) {

            if (this.interactiveDrawables[i].contains(mx, my)) {

                if (this.selection && (this.selection.type !==  this.interactiveDrawables[i].type)) {
                    this.removeAllSelections();
                    return;
                }

                if (this.interactiveDrawables[i].type === 'resolvable') {
                    this.interactiveDrawables[i].connectorSelected(mx,my);

                    if (this.selection && this.selection.connectorSelection && this.interactiveDrawables[i] && this.interactiveDrawables[i].connectorSelection) {
                        if (this.selection.connectorSelection.type === 'in' && this.interactiveDrawables[i].connectorSelection.type === 'out') {
                            this.connectResolvableElements(true,this.selection.elementId,this.selection.connectorSelection.num,
                                this.interactiveDrawables[i].elementId,this.interactiveDrawables[i].connectorSelection.num);
                            this.removeAllSelections();
                            return;

                        } else if (this.selection.connectorSelection.type === 'out' && this.interactiveDrawables[i].connectorSelection.type === 'in') {
                            this.connectResolvableElements(true,this.interactiveDrawables[i].elementId,this.interactiveDrawables[i].connectorSelection.num,
                                this.selection.elementId,this.selection.connectorSelection.num);
                            this.removeAllSelections();
                            return;
                        }
                    }
                } else if (this.interactiveDrawables[i].type === 'unresolvable') {
                    this.interactiveDrawables[i].connectorSelected(mx,my);

                    if (this.selection && this.selection.connectorSelection && this.interactiveDrawables[i] && this.interactiveDrawables[i].connectorSelection) {
                        this.connectUnResolvableElements(false,this.selection.elementId,this.selection.connectorSelection,
                            this.interactiveDrawables[i].elementId,this.interactiveDrawables[i].connectorSelection);
                    }

                }


                var mySel = this.interactiveDrawables[i];
                this.dragstartx = mySel.x;
                this.dragstarty = mySel.y;
                this.dragoffx = mx - mySel.x;
                this.dragoffy = my - mySel.y;
                this.dragging = true;
                this.selection = mySel;
                this.dirty = true;
                this.controls.updateVisibility();
                return;
            }
        }
        if (this.selection) {
            this.removeAllSelections();
        }
        this.controls.updateVisibility();
    },

    removeAllSelections: function() {
        for (key in this.interactiveDrawables) {
            this.interactiveDrawables[key].connectorSelection = null;
        }
        this.selection.connectorSelection = null;
        this.selection = null;
        this.dirty = true;
    },

    mousemove: function(e) {
        if (this.dragging){
            var mouse = this.getMouse(e);
            this.selection.x = mouse.x - this.dragoffx;
            this.selection.y = mouse.y - this.dragoffy;
            this.dirty = true;
        }
    },

    mouseup: function(e) {
        var that= this;
        if (this.dragging && this.selection !== null &&
            this.dragstartx !== this.selection.x && this.dragstarty !== this.selection.y) {
            this.interact(
                'edit/move_element',
                {
                    element_id:this.selection.elementId,
                    pos:{x:this.selection.x ,y:this.selection.y}
                },
                function(msg) {
                    that.editor.treeObject.setDirty();
                });
        }
        this.dragging = false;
    }
};
var interactiveUnResolvableElement = {
    type: 'unresolvable',
    canvas:null,
    elementId:null,
    identification:null,
    title: null,
    x:null,
    y:null,
    w:150,
    h:120,
    fill:null,
    strokeColor: '#000000',
    titleFont: '15px Arial',
    titleFontHeight: 15,
    titleStrokeColor: 'darkred',
    connectorFont: '9px Arial',
    connectorFontHeight: 9,
    connectorWidth:20,
    connectorHeight:20,
    titleHeight:20,
    connectorSelection:null,
    connections: {
        'north' : {'elementId':null,'direction':null,'type':null},
        'south' : {'elementId':null,'direction':null,'type':null},
        'west' : {'elementId':null,'direction':null,'type':null},
        'east' : {'elementId':null,'direction':null,'type':null}
    },
    init: function(canvas,title,identification,x,y,fill) {
        this.canvas = canvas;
        this.title = title || 'no name';
        this.identification = identification || 'unknown';
        this.x = x || 10;
        this.y = y || 10;
        this.fill = fill || '#AAAAAA';
    },
    draw: function(ctx) {
        var x = this.x;
        var y = this.y;

        //draw outbounds
        ctx.strokeStyle = this.strokeColor;
        ctx.fillStyle = this.fill;
        this.canvas.drawRoundRect(x + 0.5,y + 0.5,this.w,this.h,10,true,true);

        //draw title
        ctx.fillStyle = this.titleStrokeColor;
        ctx.font = this.titleFont;
        ctx.textAlign="center";
        ctx.fillText(this.title , x + (this.w /2) , y + (this.titleHeight / 2 + this.titleFontHeight / 2) + ((this.h / 2) - this.titleHeight / 2 ));

        //draw connectors:
        //north:
        ctx.beginPath();
        ctx.rect(x + (this.w /2) - (this.connectorWidth / 2) + 0.5, y + 0.5, this.connectorWidth,this.connectorHeight);
        ctx.stroke();
        //south:
        ctx.beginPath();
        ctx.rect(x + (this.w /2) - (this.connectorWidth / 2) + 0.5, y + this.h - this.connectorHeight + 0.5, this.connectorWidth,this.connectorHeight);
        ctx.stroke();
        //west:
        ctx.beginPath();
        ctx.rect(x + 0.5, y + ( this.h / 2) - (this.connectorHeight / 2) + 0.5, this.connectorWidth,this.connectorHeight);
        ctx.stroke();
        //east:
        ctx.beginPath();
        ctx.rect(x + this.w - this.connectorWidth + 0.5, y + ( this.h / 2) - (this.connectorHeight / 2) + 0.5, this.connectorWidth,this.connectorHeight);
        ctx.stroke();

        ctx.textAlign="left";
    },
    contains: function(mx,my) {
        return  (this.x <= mx) && (this.x + this.w >= mx) &&
            (this.y <= my) && (this.y + this.h >= my);
    },
    connectorSelected: function(mx,my) {
        if (mx > (this.x + (this.w /2) - (this.connectorWidth / 2)) &&
            mx < (this.x + (this.w /2) - (this.connectorWidth / 2) + this.connectorWidth) &&
            my > this.y &&
            my < (this.y + this.connectorHeight)) {
            this.connectorSelection = "north";
        } else
            if (mx > (this.x + (this.w /2) - (this.connectorWidth / 2)) &&
                mx < (this.x + (this.w /2) - (this.connectorWidth / 2) + this.connectorWidth) &&
                my > (this.y + this.h - this.connectorHeight) &&
                my < (this.y + this.h)) {
                this.connectorSelection = "south";
        } else
            if (mx > this.x &&
                mx < (this.x + this.connectorWidth) &&
                my > (this.y + ( this.h / 2) - this.connectorHeight /2) &&
                my < (this.y + ( this.h / 2) + this.connectorHeight/2)) {
                this.connectorSelection = 'west';
        } else
            if (mx > this.x + this.w - this.connectorWidth &&
                mx < (this.x + this.w) &&
                my > (this.y + ( this.h / 2) - this.connectorHeight /2) &&
                my < (this.y + ( this.h / 2) + this.connectorHeight/2)) {
                this.connectorSelection = 'east';
            }
    },
    getConnectorPosition: function(direction) {
        if (direction === 'north') {
            vector2.x = this.x + this.w / 2;
            vector2.y = this.y;
        } else if (direction === 'south') {
            vector2.x = this.x + this.w / 2;
            vector2.y = this.y + this.h;
        } else if (direction === 'west') {
            vector2.x = this.x;
            vector2.y = this.y + this.h / 2;
        } else if (direction === 'east') {
            vector2.x = this.x + this.w;
            vector2.y = this.y + this.h / 2;
        }
    },
    getConnection: function(direction){
        return this.connections[direction];
    },
    setConnection: function(direction,targetElementId,targetDirection,type){
        this.connections[direction] = {'elementId':targetElementId,'direction':targetDirection,'type':type};
        console.log(this.connections);
    },
}
var interactiveResolvableElement = {
    type: 'resolvable',
    canvas:null,
    elementId:null,
    identification:null,
    title: null,
    inputCount:null,
    outputCount:null,
    inputs: [],
    outputs: [],
    x:null,
    y:null,
    w:150,
    h:null,
    fill:null,
    strokeColor: '#000000',
    titleFont: '15px Arial',
    titleFontHeight: 15,
    titleStrokeColor: 'darkred',
    connectorFont: '9px Arial',
    connectorFontHeight: 9,
    titleHeight:20,
    connectorHeight: 15,
    connectorWidth: 65,
    connectorPadding:10,
    connectorSelection:null,
    inputNames: [],
    outputNames: [],
    init: function(canvas,title,identification,inputCount,outputCount,x,y,inputNames,outputNames,fill) {
        this.canvas = canvas;
        this.title = title || 'no name';
        this.identification = identification || 'unknown';
        this.inputCount = inputCount || 0;
        this.outputCount = outputCount || 0;
        this.x = x || 10;
        this.y = y || 10;
        this.fill = fill || '#AAAAAA';
        this.inputs = [];
        this.outputs = [];
        this.inputNames = inputNames || [];
        this.outputNames = outputNames || [];

        var mh = this.inputCount > this.outputCount ? this.inputCount : this.outputCount;
        this.h = (mh * (this.connectorHeight + 2 * this.connectorPadding)) + this.titleHeight;

        for (var c = 1; c <= this.inputCount;c++) {
            this.setConnectorInput(c);
        }
        for (var c = 1; c <= this.outputCount;c++) {
            this.setConnectorOutput(c);
        }
    },
    setConnectorInput: function(num,elementId,channel) {
        var key = null;
        if (typeof elementId !== 'undefined') {
            key = Object.create(destination);
            key.type='out';
            key.elementId = elementId;
            key.num = channel;
        }
        this.inputs[num-1] = key;
    },
    setConnectorOutput: function(num,elementId,channel) {
        var key = null;
        if (typeof elementId !== 'undefined') {
            key = Object.create(destination);
            key.type='in';
            key.elementId = elementId;
            key.num = channel;
        }
        this.outputs[num-1] = key;
    },
    getConnectorInput: function(num) {
        return this.inputs[num-1];
    },
    getConnectorOutput: function(num) {
        return this.outputs[num-1];
    },
    draw: function(ctx) {
        var x = this.x;
        var y = this.y;

        //draw outbounds
        ctx.strokeStyle = this.strokeColor;
        ctx.fillStyle = this.fill;
        this.canvas.drawRoundRect(x + 0.5,y + 0.5,this.w,this.h,10,true,true);

        //draw title:
        ctx.beginPath();
        ctx.moveTo(x + 0.5,y + this.titleHeight + 0.5);
        ctx.lineTo(x + this.w + 0.5,y + this.titleHeight + 0.5);
        ctx.stroke();
        ctx.fillStyle = this.titleStrokeColor;
        ctx.font = this.titleFont;
        ctx.textAlign="center";
        ctx.fillText(this.title /*+ ' ' + this.elementId*/, x + (this.w /2) , y + (this.titleHeight / 2 + this.titleFontHeight / 2));

        //draw inputs:
        var start = y + this.titleHeight;
        ctx.textAlign="left";
        ctx.fillStyle = this.strokeColor;
        for (var c = 1; c <= this.inputCount;c++) {
            ctx.beginPath();
            ctx.rect(x + 0.5,start + this.connectorPadding + 0.5, this.connectorWidth,this.connectorHeight);
            ctx.stroke();
            ctx.fillStyle = this.strokeColor;
            ctx.font = this.connectorFont;
            var text = typeof this.inputNames[c-1] !== 'undefined' ? this.inputNames[c-1] : 'In';
            ctx.fillText(text, x + 2, start + this.connectorPadding + (this.connectorHeight / 2 + this.connectorFontHeight / 2));
            ctx.stroke();
            start += 2 * this.connectorPadding + this.connectorHeight;
        }
        //draw outputs:
        var start = y + this.titleHeight;
        ctx.textAlign="right";
        ctx.fillStyle = this.strokeColor;
        for (var c = 1; c <= this.outputCount;c++) {
            ctx.beginPath();
            ctx.rect(x + this.w - this.connectorWidth + 0.5,start + this.connectorPadding + 0.5, this.connectorWidth,this.connectorHeight);
            ctx.stroke();
            ctx.fillStyle = this.strokeColor;
            ctx.font = this.connectorFont;
            var text = typeof this.outputNames[c-1] !== 'undefined' ? this.outputNames[c-1] : 'Out';
            ctx.fillText(text, x + this.w - 2, start + this.connectorPadding + (this.connectorHeight / 2 + this.connectorFontHeight / 2));
            ctx.stroke();
            start += 2 * this.connectorPadding + this.connectorHeight;
        }

        ctx.textAlign="left";
    },
    contains: function(mx,my) {
        return  (this.x <= mx) && (this.x + this.w >= mx) &&
            (this.y <= my) && (this.y + this.h >= my);
    },
    connectorSelected: function(mx,my) {
        this.connectorSelection = null;
        var start = this.y + this.titleHeight;
        for (var c = 1; c <= this.inputCount;c++) {
            if ((this.x <= mx) && (this.x + this.connectorWidth >= mx) &&
                (start + this.connectorPadding <= my) && (start + this.connectorPadding + this.connectorHeight >= my)) {
                this.connectorSelection = Object.create(destination);
                this.connectorSelection.type='in';
                this.connectorSelection.elementId = this.elementId;
                this.connectorSelection.num = c;
            }
            start += 2 * this.connectorPadding + this.connectorHeight;
        }
        var start = this.y + this.titleHeight;
        for (var c = 1; c <= this.outputCount;c++) {
            if ((this.x + this.w - this.connectorWidth <= mx) && (this.x + this.w - this.connectorWidth + this.connectorWidth >= mx) &&
                (start + this.connectorPadding <= my) && (start + this.connectorPadding + this.connectorHeight >= my)) {
                this.connectorSelection = Object.create(destination);
                this.connectorSelection.type='out';
                this.connectorSelection.elementId = this.elementId;
                this.connectorSelection.num = c;
            }
            start += 2 * this.connectorPadding + this.connectorHeight;
        }
        return this.connectorSelection;
    },
    getConnectorInPosition: function(num) {
        vector2.x = this.x - 1;
        vector2.y = this.y + this.titleHeight + (num * (2 * this.connectorPadding + this.connectorHeight )) - this.connectorPadding - (this.connectorHeight / 2);
    },
    getConnectorOutPosition: function(num) {
        vector2.x = this.x + this.w + 1;
        vector2.y = this.y + this.titleHeight + (num * (2 * this.connectorPadding + this.connectorHeight )) - this.connectorPadding - (this.connectorHeight / 2);
    }
};

var gridBackground = {
    canvas:null,
    widthDistance:50,
    heightDistance:50,
    backgroundColor:null,
    lineColor: null,
    init: function(canvas,wd,hd,backgroundColor, lineColor) {
        this.canvas = canvas;
        this.widthDistance = wd;
        this.heightDistance = hd;
        this.backgroundColor = backgroundColor;
        this.lineColor = lineColor;
    },
    draw: function(ctx) {
        ctx.fillStyle = this.backgroundColor;
        ctx.fillRect(0,0,this.canvas.width,this.canvas.height);

        ctx.beginPath();
        ctx.strokeStyle = this.lineColor;
        for(var x = 0;x < this.canvas.width;x=x+this.widthDistance) {
            ctx.moveTo(x + 0.5 ,0.5);
            ctx.lineTo(x + 0.5,this.canvas.height + 0.5);
        }
        for(var y = 0;y < this.canvas.height;y=y+this.heightDistance) {
            ctx.moveTo(0.5,y + 0.5);
            ctx.lineTo(this.canvas.width + 0.5,y + 0.5);
        }
        ctx.stroke();
    }
};

var vector2 = {
    x:0,
    y:0
};

var destination = {
    elementId:0,
    type:'',
    num:0
};

/*var module = {
    identification: '',
    inputCount: 0,
    outputCount: 0,
    inputNames: [],
    outputNames: [],
    editable: false,
    isresolvable: false,
    init: function(identification,inputCount,outputCount,inputNames,outputNames,editable,isresolvable) {
        this.identification = identification;
        this.inputCount     = inputCount;
        this.outputCount    = outputCount;
        this.inputNames     = inputNames;
        this.outputNames    = outputNames;
        this.editable       = editable;
        this.isresolvable  = isresolvable;
    }
};*/
var module = {
    identification: '',
    /*inputCount: 0,
    outputCount: 0,
    inputNames: [],
    outputNames: [],*/
    options: [],
    editable: false,
    isresolvable: false,
    init: function(options) {
        this.identification = options.name;
        /*this.inputCount     = inputCount;
        this.outputCount    = outputCount;
        this.inputNames     = inputNames;
        this.outputNames    = outputNames;*/
        this.editable       = options.editable;
        this.isresolvable  = options.isresolvable;
        this.options = options;
    }
};


var VisualProcessGenericControlContainer = {

    controlContainer:null,
    controls: null,
    template: null,
    updateVisibility: null,
    initDone: null,

    init: function(selector,template,updateVisibility) {

        if (this.initDone) {
            return;
        }

        var that = this;

        this.controlContainer = $(selector);
        this.controls = [];
        this.template = template || '<ul></ul>';
        this.updateVisibility = updateVisibility || function(){};
        this.controlContainer.on('click','ul li[clickable="clickable-function"]',function(e,i){
            that.calls($(this).data('function'));
        });
        this.initDone = true;
    },

    addControl: function (functionname, context, callback, icon, title, additional) {
        this.controls.push({'functionname':functionname,'context':context,'function':callback,'icon':icon,'title':title,'additional': additional});
    },

    clearControls: function() {
      this.controls = [];
    },

    build: function() {

        this.controlContainer.html('');
        this.controlContainer.append(this.template);
        for(var i=0;i<this.controls.length;i++) {
            this.controlContainer.find('ul').append('<li title="'+this.controls[i].title+'" clickable="none-clickable-function" data-function="'+this.controls[i].functionname+'"><i class="'+this.controls[i].icon+'"></i></li>');
        }
        var that = this;

    },

    calls: function(functionname) {
        var that = this;
        for(var i=0;i<that.controls.length;i++) {
            if (that.controls[i].functionname === functionname) {
                if (typeof that.controls[i].function === 'function') {
                    that.controls[i].function(that.controls[i].context);
                }
            }
        }
    }
};

var inArray = function(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
};

