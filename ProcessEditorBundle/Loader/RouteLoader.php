<?php

namespace Missbach\ProcessEditorBundle\Loader;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Routeloader
 * @package Missbach\ProcessBundle\Loader
 */
class RouteLoader extends Loader
{
    const NAME_PROCESS_ROUTES = 'process_routes';

    /**
     * @var bool
     */
    protected $isLoaded = false;

    /**
     * @param mixed $resource
     * @param null $type
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();

        $importedRoutes = $this->import('@MissbachProcessEditorBundle/Resources/config/routing.yaml', 'yaml');

        $collection->addCollection($importedRoutes);

        return $collection;
    }

    /**
     * @param mixed $resource
     * @param null $type
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return self::NAME_PROCESS_ROUTES === $type;
    }
}