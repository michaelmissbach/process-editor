<?php

namespace Missbach\ProcessEditorBundle\Controller;

use Missbach\ProcessBundle\Core\Exceptions\NotImplementedException;
use Missbach\ProcessBundle\Core\Interfaces\IResolveable;
use Missbach\ProcessBundle\Services\ProcessService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EditorController extends Controller
{
    /**
     * @param Request $request
     * @param string $containerID
     * @return Response
     */
    public function testProcessAction(Request $request)
    {
        return new JsonResponse(['result'=>$this->get('process.service')->executeProcessById(ProcessService::getCurrentContainerId(),true)]);
    }

    /**
     * @param Request $request
     * @param string $containerID
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $firstLoad = $this->get('process.core.container')->hasSessionData();

        $this->get('process.core.container')->load();
        $this->get('process.core.factory');

        return $this->render('@MissbachProcessEditor/editor/index.html.twig', [
            'container_id' => null,
            'first_load' => !$firstLoad
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function addElementAction(Request $request)
    {
        $this->get('process.core.container')->load();
        /** @var IResolveable $elem */
        $elem = $this->get('process.core.factory')->getByName($request->get('name'));
        $elem->__setProcessMeta('objMetas',$request->get('pos')['x']);
        $elem->__setProcessMeta('posx',$request->get('pos')['x']);
        $elem->__setProcessMeta('posy',$request->get('pos')['y']);

        $id = $this->get('process.core.container')->add(ProcessService::getCurrentContainerId(),$elem);

        return new JsonResponse(['id'=>$id]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function moveElementAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $elem = $this->get('process.core.container')->getElementByContainerIdAndElementId(ProcessService::getCurrentContainerId(),$request->get('element_id'));
        $elem->__setProcessMeta('posx',$request->get('pos')['x']);
        $elem->__setProcessMeta('posy',$request->get('pos')['y']);
        $this->get('process.core.container')->addElementByContainerId(ProcessService::getCurrentContainerId(),$request->get('element_id'),$elem);
        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function connectElementsAction(Request $request)
    {
        $this->get('process.core.container')->load();

        $result = $this->get('process.core.container')
            ->connectElements(
                ProcessService::getCurrentContainerId(),
                $request->get('elemId1'),
                $request->get('outputChannel',null),
                $request->get('elemId2'),
                $request->get('inputChannel',null)
                );
        return new JsonResponse(['result'=>$result]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function unconnectElementsAction(Request $request)
    {
        $this->get('process.core.container')->load();

        $result = $this->get('process.core.container')
            ->unconnectElements(
                ProcessService::getCurrentContainerId(),
                $request->get('elemId1'),
                $request->get('outputChannel',null),
                $request->get('elemId2'),
                $request->get('inputChannel',null)
                );
        return new JsonResponse(['result'=>$result]);
    }

    /**
     * @param Request $request
     */
    public function removeElementAction(Request $request)
    {
        $this->get('process.core.container')->load();

        $result = $this->get('process.core.container')->removeElementById(
            ProcessService::getCurrentContainerId(),
            $request->get('elemId')
        );
        $script = $this->get('process.editor.service')->getBuildedContainerJs(ProcessService::getCurrentContainerId());

        return new JsonResponse(['result'=>$result,'script'=>$script]);
    }

    /**
     * @param Request $request
     */
    public function loadAction(Request $request)
    {
        $this->get('process.core.container')->load();

        $script = $this->get('process.editor.service')->getBuildedContainerJs(ProcessService::getCurrentContainerId());

        return new JsonResponse(['script'=>$script]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeLoadAllAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $tree = $this->get('process.core.container')->getTreeForForFrontend();

        return new JsonResponse($tree);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeAddAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $this->get('process.core.container')->addNewContainerByParentId(ProcessService::getCurrentContainerId());
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeRemoveAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $this->get('process.core.container')->removeRecursiveInTree(ProcessService::getCurrentContainerId());
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeRenameAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $this->get('process.core.container')->renameContainer(
            ProcessService::getCurrentContainerId(),
            $request->get('shown_name'),
            $request->get('call_name')
        );
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeSaveAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $this->get('process.core.container')->saveContainerAndOverwriteUnsafedState(ProcessService::getCurrentContainerId());
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function treeLoadAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $this->get('process.core.container')->loadContainerAndOverwriteUnsafedState(ProcessService::getCurrentContainerId());
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editorLoadAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $elem = $this->get('process.core.container')->getElementByContainerIdAndElementId(ProcessService::getCurrentContainerId(),$request->get('element_id'));
        return new JsonResponse(['form'=>$elem->__getEditor()]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editorSaveAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $elem = $this->get('process.core.container')->getElementByContainerIdAndElementId(ProcessService::getCurrentContainerId(),$request->get('element_id'));
        $elem->__setConfig($request->get('properties'));
        $this->get('process.core.container')->addElementByContainerId(ProcessService::getCurrentContainerId(),$request->get('element_id'),$elem);
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function elementsLoadAction(Request $request)
    {
        $this->get('process.core.container')->load();
        $template = $this->renderView('@MissbachProcessEditor/editor/elements.html.twig');
        return new JsonResponse(['form'=>$template]);
    }
}
